class LoginAmaro
  include HTTParty
  base_uri 'https://dev.amaro.com/shop/api'

  def body(credencials)
    @body = {
        "emailOrCpf": data(credencials, "email"),
        "password": data(credencials, "pass")
    }
    return @body
  end
  
  def login(credencials)
    self.class.post("/_account/login", body(credencials))
  end

  def data(credencial, key)
    credencial = credencial.gsub(" ", "_")
    credencial = CREDENCIALS[credencial.to_sym]
    return credencial = credencial[key.to_sym]
  end

end