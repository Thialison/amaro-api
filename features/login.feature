Feature: Login
    As a user
    I want login on Amaro

    Background: Access API login address
        Given I access the API to login

    Scenario: Login on Amaro with a valid login
        When I login with "valid login"
        Then I should be logged on amaro

    Scenario: Login on Amaro with invalid password
        When I login with "invalid password"
        Then I should see an message error