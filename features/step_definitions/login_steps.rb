Given("I access the API to login") do
    @login = LoginAmaro.new
end

When("I login with {string}") do |credencials|
    @response = @login.login(credencials)
end

Then("I should be logged on amaro") do
    status_code = @response.code
    expect(status_code).to be(200)
end

Then("I should see an message error") do
    status_code = @response.code
    expect(status_code).to be(200)  
end